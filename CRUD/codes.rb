#!/usr/bin/ruby
# frozen_string_literal: true

require 'mysql2'
load 'conf.rb'

Bdd = Base.new
Bdd.init
OpenConnectBdd = Mysql2::Client.new(host: Bdd.host,
                                    database: Bdd.database,
                                    user: Bdd.user,
                                    password: Bdd.pass)

# CREATE
# Function addCode -> Insert code into
# Parameters : (str)URL, (str)MERCHANT
def addCode(url, merchant)
  result_object = OpenConnectBdd.prepare('INSERT INTO codes(code_text, merchant, expiration_date) VALUES(?, ?, ?)')
  result_object.execute(url, merchant, Date.today + 365)
end

# READ
# Function getCodes -> Select codes
# Return : JSON
def getCodes
  result_object = OpenConnectBdd.query('SELECT * FROM codes')
  hash = result_object.each(&:to_h)
  hash.to_json
end

# Function getCodeById -> Select code depending on an id_code
# Parameter : (int)id_code
# Return : JSON
def getCodeById(id)
  result_object = OpenConnectBdd.prepare('SELECT * FROM codes WHERE id_code = ?')
  result_object = result_object.execute(id)
  hash = result_object.each(&:to_h)
  hash.to_json
end

