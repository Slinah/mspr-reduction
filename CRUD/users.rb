#!/usr/bin/ruby
# frozen_string_literal: true

require 'bcrypt'
require 'mysql2'
include BCrypt
load 'conf.rb'

Bdd = Base.new
Bdd.init
OpenConnectBdd = Mysql2::Client.new(host: Bdd.host,
                                    database: Bdd.database,
                                    user: Bdd.user,
                                    password: Bdd.pass)

# CREATE
# Function addUser -> Insert an User into database
# Parameters : (str)MAIL, (str)PASS, (date)BIRTH
# Return : HTTP body
def addUser(mail, pass, birth)
  if m_exist(mail).zero?
    result_object = OpenConnectBdd.prepare('INSERT INTO users(email, password, birth_date) VALUES(?, ?, ?)')
    # Using BCrypt to hash the password
    result_object.execute(mail, Password.create(pass), birth)
    status 200
    "Compte créer pour le mail : #{mail}"
  else
    status 400
    "Compte avec déjà existant avec cet email : #{mail}"
  end
end

# READ
# Function getUserById -> Select an User into database depending on the id_user
# Parameter : (int)id_user
# Return : JSON
def getUserById(id)
  result_object = OpenConnectBdd.prepare('SELECT * FROM users WHERE id_user = ?')
  result_object = result_object.execute(id)
  hash = result_object.each(&:to_h)
  hash.to_json
end

# Function getUserByMailPass -> Authorize the connection to front depending ont a mail and a password
# Parameter : (string)MAIL, (string)PASS
# Return :token
def getUserByMailPass(mail, pass)
  result_object = OpenConnectBdd.prepare('SELECT email, password FROM users WHERE email = ?')
  result_object = result_object.execute(mail)
  hash = result_object.each(&:to_h)
  if !hash.empty?
    if Password.new(hash[0]['password']) == pass
      body SecureRandom.alphanumeric
      status 200
    else
      body 'Les informations de connexion sont incorrectes'
      status 400
    end
  else
    body 'Les informations de connexion sont incorrectes'
    status 400
  end
end

# UPDATE
# Function updateUserById -> Update an user from database depending on id_user
# Parameters :
# Return : HTTP body
def updateUserById(mail, pass, birth, id)
  request_keys_mail = mail unless mail.eql? ''
  request_keys_pass = Password.create(pass) unless pass.eql? ''
  request_keys_birth = birth unless pass.eql? ''
  result_object = OpenConnectBdd.prepare('UPDATE users SET email = ?, password = ?, birth_date = ? WHERE id_user = ?')
  result_object.execute(request_keys_mail, request_keys_pass, request_keys_birth, id)
  status 200
  body 'Update effectué avec succès'
end

# DELETE
# Function deleteUserById -> Delete an user from database depending on id_user
# Parameter : (int)id_user
# Return : HTTP body
def deleteUserById(id)
  if i_exist(id).positive?
    result_object = OpenConnectBdd.prepare('DELETE FROM users WHERE id_user = ?')
    result_object.execute(id)
    status 200
    "Suppression de l'utilisateur réussie."
  else
    status 404
    "Le compte utilisateur avec l'id #{id} n'existe pas dans la base."
  end
end

# Tests
# Function m_exist -> Check if an user exist with a specified mail
# Parameter : (string)MAIL
# Return : (int)number of lignes
def m_exist(mail)
  result_object = OpenConnectBdd.prepare('SELECT COUNT(*) AS ligne FROM users WHERE email = ?')
  result_object = result_object.execute(mail)
  func_result = ''
  result_object.each { |i| func_result = i['ligne'] }
  func_result
end

# Function i_exist -> Check if an user exist with a specified id_user
# Parameter : (int)id_user
# Return : (int)number of lignes
def i_exist(id)
  result_object = OpenConnectBdd.prepare('SELECT COUNT(*) AS ligne FROM users WHERE id_user = ?')
  result_object = result_object.execute(id)
  func_result = ''
  result_object.each { |i| func_result = i['ligne'] }
  func_result
end
