#!/usr/bin/ruby
# frozen_string_literal: true

require 'mysql2'
load 'conf.rb'

Bdd = Base.new
Bdd.init
OpenConnectBdd = Mysql2::Client.new(host: Bdd.host,
                                    database: Bdd.database,
                                    user: Bdd.user,
                                    password: Bdd.pass)
# READ
# Function getUsedCodesByIdUser -> Select a list of codes used by current user
# Parameter : (int)id_user
# Return : JSON
def getUsedCodesByIdUser(id)
  result_object = OpenConnectBdd.prepare('SELECT * FROM codes c JOIN user_codes uc ON c.id_code=uc.id_code WHERE uc.id_user = ?')
  result_object = result_object.execute(id)
  hash = result_object.each(&:to_h)
  hash.to_json
end
