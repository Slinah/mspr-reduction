# frozen_string_literal: true

require 'cuba'

Cuba.define do
  def read_fixture()
    #File.read [File.dirname(__FILE__), '..', 'fixtures', *path].join('/')
    # File.read 'unitary/fixtures/posts/1.json'
    File.read 'unitary/fixtures/get/codes_spec.rb.json'
  end

  #on get do
  #  on 'posts/:id' do |id|
  #    res.write read_fixture('posts', id)
  #  end
  #end
  on get do
    on 'codes_spec.rb' do
      res.write read_fixture
    end
  end
end

# If this file is loaded from the command line, start the server
Rack::Handler::WEBrick.run(Cuba) if $PROGRAM_NAME == __FILE__
