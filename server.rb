#!/usr/bin/ruby
# frozen_string_literal: true

require 'sinatra'
load 'CRUD/codes.rb'
load 'CRUD/users.rb'
load 'CRUD/users_codes.rb'

# ROUTES CODES

# Route {get}/codes_spec.rb, call getCodes with no parameter
get '/codes' do
  getCodes
end

# Route {get}/code/{id}, call getCodeById with one parameter : (int)id_code
get '/code/:id' do |i|
  getCodeById(i)
end

# Route {post}/code?url={url}&merchant={merchant}, call addCode with two parameters : (str)URL and (str)MERCHANT
post '/code' do
  addCode(params[:url], params[:merchant])
end

# ROUTES USERS

# Route {get}/user/{id}, call getUserById with one parameter : (int)id_user
get '/user/:id' do |i|
  getUserById(i)
end

# Route {delete}/user/{id}, call deleteUserById with one parameter : (int)id_user
delete '/user/:id' do |i|
  deleteUserById(i)
end

# Route {get}/user?email={email}&pass={pass}, call getUserByMailPass with two parameters : (str)EMAIL and (str)PASS
# => TO FIX
post '/usercredential' do
  getUserByMailPass(params[:email], params[:pass])
end

# Route {post}/user?email={email}&pass={pass}&birth={birth}, call addUser with three parameters : (str)EMAIL, (str)PASS and (date)BIRTH
post '/user' do
  addUser(params[:email], params[:pass], params[:birth])
end

# Route {put}/user?email={email}&pass={pass}&birth={birth}&id={id}
put '/user' do
  updateUserById(params[:email], params[:pass], params[:birth], params[:id])
end

# ROUTES USERS_CODES

# Route {get}/usercodes/{id}, call getUsedCodesByIdUser with one parameter : (int)id_user
get '/usercodes/:id' do |i|
  getUsedCodesByIdUser(i)
end